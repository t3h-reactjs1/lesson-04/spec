1. Slide:
Nội dung buổi tới mình sẽ tìm hiểu về thuộc tính display và các giá trị của thuộc tính này, nhưng mình sẽ dành nhiều thời gian để tìm hiểu về flex (1 giá trị của thuộc tính display), flex sẽ giúp dàn layout, căn chỉnh vị trí các phần tử 1 cách nhanh chóng.

https://docs.google.com/presentation/d/1seL_b9S5enjPNnfoeFvVkehD58nmKtyuhf8qw26Vbc4/edit#slide=id.p

2. Web reference:

https://quantrimang.com/hoc/su-dung-bo-cuc-trang-flexbox-trong-css-163636

https://codepen.io/enxaneta/full/adLPwv/?zarsrc=411&utm_source=zalo&utm_medium=zalo&utm_campaign=zalo

3. Font & icon import
https://fontawesome.com/v6/search

    <link
        rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css"
        integrity="sha512-z3gLpd7yknf1YoNbCzqRKc4qyor8gaKU1qmn+CShxbuBusANI9QpRohGBreCFkKxLhei6S9CQXFEbbKuqLg0DA=="
        crossorigin="anonymous"
        referrerpolicy="no-referrer"
	/>